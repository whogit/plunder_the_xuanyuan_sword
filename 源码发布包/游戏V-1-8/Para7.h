void CMainFrame::Para7Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para7.dlg");//调入对话
	LoadActor("pic\\hero.bmp");//调入主角
	deleteallbmpobjects();
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic300.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic302.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic301.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic320.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic315.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（1）
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic309.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//哈德尔
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic313.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（4）
	hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic321.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//洛桑
	hbmp_Photo9=(HBITMAP)LoadImage(NULL,"pic\\pic325.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//阿萨德
/////////////////////////////////////////////////////////////////////////////
//调入所需道具
	::DeleteObject(hbmp_Prop0);//释放内存
	::DeleteObject(hbmp_Prop1);//释放内存
	::DeleteObject(hbmp_Prop2);//释放内存
	::DeleteObject(hbmp_Prop3);//释放内存
	::DeleteObject(hbmp_Prop4);//释放内存
	::DeleteObject(hbmp_Prop5);//释放内存
	::DeleteObject(hbmp_Prop6);//释放内存
	::DeleteObject(hbmp_Prop7);//释放内存
	::DeleteObject(hbmp_Prop8);//释放内存
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\hero.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic107.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic108.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//老头
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\pic100.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic116.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//阿萨德
	hbmp_5=(HBITMAP)LoadImage(NULL,"pic\\pic102.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//CIA分子
	hbmp_6=(HBITMAP)LoadImage(NULL,"pic\\pic117.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//恐怖分子
	hbmp_7=(HBITMAP)LoadImage(NULL,"pic\\pic101.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_8=(HBITMAP)LoadImage(NULL,"pic\\pic110.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//白莲教组图
	hbmp_9=(HBITMAP)LoadImage(NULL,"pic\\pic021.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//军舰
	hbmp_10=(HBITMAP)LoadImage(NULL,"pic\\pic210.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//汗叹倒
/////////////////////////////////////////////////////////////////////////////
//引入开始游戏的位置
/////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para7Info(int type)
{
	int i;

if(st_dialog==20)
{//行走状态的陷阱。
	m_info_st=8;
	m_oldinfo_st=8;
	SetActTimer();
	st_dialog=2000;
}
else if(st_dialog==21)
{//战斗状态的陷阱。
	if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
		m_info_st=100;m_oldinfo_st=100;
	}else{
		m_info_st=10;m_oldinfo_st=10;
	}
	SetActTimer();
	st_dialog=2000;
}
else if(st_sub1==0){
	if(st_dialog==0){
		m_info_st=9;
		m_oldinfo_st=9;
		OpenMap("map\\maps105.bmp","map\\map105c.map","map\\map105b.npc");
		current.x=160;
		current.y=172;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		vopen();
		PlayMidi("midi\\p1016.mid");
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_9);//军舰
		resetblt();
		for(i=0;i<150;i++){//移动
			blt((800-(i*4)),300,600,180,0,180,0,0,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,11,0,0,1);
		st_dialog=1;
	}
	else if(st_dialog==1){
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_6);//恐怖分子
		resetblt();
		int j;
		for(j=4;j>0;j--){
			for(i=0;i<(4+j*2);i++){//移动
				blt(320,(344-(i*16)),32,32,64,128,64,32,pdc);
				GameSleep(20);
				blt(320,(336-(i*16)),32,32,160,128,160,32,pdc);
				GameSleep(20);
			}
			resetblt();
		}
		for(j=4;j>0;j--){
			for(i=0;i<(4+j*2);i++){//移动
				blt(448,(344-(i*16)),32,32,0,128,0,32,pdc);
				GameSleep(20);
				blt(448,(336-(i*16)),32,32,96,128,96,32,pdc);
				GameSleep(20);
			}
			resetblt();
		}
		MemDC.SelectObject(hbmp_4);//阿萨德
		resetblt();
		for(j=0;j<8;j++){//移动
			blt(384,(344-(j*16)),32,32,64,64,0,64,pdc);
			GameSleep(100);
			blt(384,(336-(j*16)),32,32,96,64,32,64,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,46,0,2);
		st_dialog=2;
	}
	else if(st_dialog==2){
		Dialog(4,14,0,0,3);
		st_dialog=3;
	}
	else if(st_dialog==3){
		Dialog(4,0,46,0,4);
		st_dialog=4;
	}
	else if(st_dialog==4){
		Dialog(4,11,0,0,5);
		st_dialog=5;
	}
	else if(st_dialog==5){
		Dialog(4,0,46,0,6);
		st_dialog=6;
	}
	else if(st_dialog==6){
		Dialog(4,11,0,0,7);
		st_dialog=7;
	}
	else if(st_dialog==7){
		Dialog(4,0,46,0,8);
		st_dialog=10;
	}
	else if(st_dialog==10){//转入荻娜处
		vclose();
		blood=200;//满血
		PlayMidi("midi\\p1004.mid");
		OpenMap("map\\maps103.bmp","map\\map105b.map","map\\map105b.npc");
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=287;///////////////
		current.x=162;
		current.y=94;
		azimuth= AZIMUTH_RIGHT;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		Dialog(4,26,0,0,9);
		st_dialog=11;
	}
	else if(st_dialog==11){
		Dialog(4,0,31,0,10);
		st_dialog=12;
	}
	else if(st_dialog==12){
		Dialog(4,0,26,0,11);
		st_dialog=13;
	}
	else if(st_dialog==13){
		Dialog(4,0,31,0,12);
		st_dialog=14;
	}
	else if(st_dialog==14){//洛桑走入
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<16;i++){//移动
			blt((i*16),208,32,48,160,144,64,144,pdc);
			GameSleep(100);
			blt((8+(i*16)),208,32,48,128,144,32,144,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,41,0,0,13);
		st_dialog=55;
	}
	else if(st_dialog==55){
		azimuth= AZIMUTH_LEFT;//卡诺回过头来
		DrawActor(0);
		Dialog(4,0,31,0,14);
		st_dialog=56;
	}
	else if(st_dialog==56){
		Dialog(4,41,0,0,15);
		st_dialog=57;
	}
	else if(st_dialog==57){
		LoadActor("pic\\hero.bmp");
		DrawActor(0);
		Dialog(2,0,0,0,16);
		st_dialog=58;
	}
	else if(st_dialog==58){
		Dialog(4,41,0,0,17);
		st_dialog=59;
	}
	else if(st_dialog==59){
		Dialog(4,41,0,0,18);
		st_dialog=60;
	}
	else if(st_dialog==60){
		Dialog(4,0,31,0,19);
		st_dialog=61;
	}
	else if(st_dialog==61){
		Dialog(4,41,0,0,20);
		st_dialog=62;
	}
	else if(st_dialog==62){
		Dialog(4,41,0,0,21);
		st_dialog=63;
	}
	else if(st_dialog==63){
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<20;i++){//洛桑走开
			blt((256-(i*16)),208,32,48,160,48,64,48,pdc);
			GameSleep(100);
			blt((248-(i*16)),208,32,48,128,48,32,48,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		PlayMidi("midi\\p1003.mid");
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;
	}
	else if(st_dialog==65)
	{
		st_sub1=1;
		st_dialog=0;
		Dialog(2,0,0,0,25);
	}
}//st_sub1=0的部分结束
else if(st_sub1==1){
	if(st_dialog==0){//把司徒从监狱里放出来的片段
		LoadActor("pic\\hero.bmp");
		m_info_st=9;
		m_oldinfo_st=9;
		OpenMap("map\\maps103.bmp","map\\map109c.map","map\\map109c.npc");
		current.x=111;
		current.y=26;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_3);//司徒
		resetblt();
		blt(400,112,32,32,64,64,0,64,&BuffDC);
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(380,260,48,48,96,624,0,624,&BuffDC);
		BuffDC.BitBlt(384,198,64,56,&CilDC,256,448,SRCCOPY);//监狱门
		vopen();
		Dialog(4,0,0,0,98);
		st_dialog=1;
	}
	else if(st_dialog==1){
		PlayWave("snd//shoot.wav");

		CDC *pdc=GetDC();
		blt(380,260,48,48,96,816,0,816,pdc);//卡诺开枪
		GameSleep(200);
		blt(380,260,48,48,96,624,0,624,pdc);
		BuffDC.BitBlt(384,198,64,56,&MapDC,384,198,SRCCOPY);//擦掉监狱门
		pdc->BitBlt(384,198,64,56,&BuffDC,384,198,SRCCOPY);
		ReleaseDC(pdc);
		pdc = NULL;

		GameSleep(200);
		Dialog(4,31,0,0,99);
		st_dialog=2;
	}
	else if(st_dialog==2){

		BuffDC.BitBlt(380,260,48,48,&MapDC,380,260,SRCCOPY);//擦掉卡诺
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(388,260,32,48,96,96,0,96,&BuffDC);//卡诺收起枪

		Dialog(4,0,3,0,100);
		st_dialog=3;
	}
	else if(st_dialog==3){
		Dialog(4,31,0,0,101);
		st_dialog=4;
	}
	else if(st_dialog==4){
		Dialog(4,0,1,0,102);
		st_dialog=5;
	}
	else if(st_dialog==5){
		Dialog(4,0,1,0,103);
		st_dialog=6;
	}
	else if(st_dialog==6){
		Dialog(4,31,0,0,104);
		st_dialog=7;
	}
	else if(st_dialog==7){
		Dialog(4,0,1,0,105);
		st_dialog=8;
	}
	else if(st_dialog==8){
		Dialog(4,0,1,0,106);
		st_dialog=9;
	}
	else if(st_dialog==9){
		vclose();
		Dialog(3,31,0,0,107);
		st_dialog=49;
	}
	else if(st_dialog==49){
		PlayMidi("midi\\p1016.mid");//中央镇，恐怖分子包围村民
		//LoadActor("pic\\hero.bmp");
		m_info_st=9;
		m_oldinfo_st=9;
		OpenMap("map\\maps105.bmp","map\\map105c.map","map\\map105b.npc");
		current.x=148;
		current.y=119;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		vopen();
		Dialog(4,0,0,0,26);
		st_dialog=50;
	}
	else if(st_dialog==50){
		Dialog(4,0,46,0,27);
		st_dialog=51;
	}
	else if(st_dialog==51){//镜头转移
		CDC *pdc=GetDC();
		for(i=0;i<15;i++){
			DrawMap();
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			current.x--;
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,46,0,28);
		st_dialog=52;
	}
	else if(st_dialog==52){
		Dialog(4,0,0,0,29);
		st_dialog=53;
	}
	else if(st_dialog==53){
		Dialog(4,0,46,0,30);
		st_dialog=60;
	}
	else if(st_dialog==60){//转移到荻娜处
		lclose();
		current.x=162;
		current.y=94;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		lopen();
		Dialog(4,0,46,0,31);
		st_dialog=61;
	}
	else if(st_dialog==61){
		Dialog(4,22,0,0,32);
		st_dialog=62;
	}
	else if(st_dialog==62){
		Dialog(4,0,46,0,33);
		st_dialog=63;
	}
	else if(st_dialog==63){
		Dialog(4,22,0,0,34);
		st_dialog=64;
	}
	else if(st_dialog==64){
		Dialog(4,0,46,0,35);
		st_dialog=65;
	}
	else if(st_dialog==65){
		Dialog(4,0,0,0,36);
		st_dialog=66;
	}
	else if(st_dialog==66){
		Dialog(4,23,0,0,37);
		st_dialog=67;
	}
	else if(st_dialog==67){
		Dialog(4,0,46,0,38);
		st_dialog=70;
	}
	else if(st_dialog==70){//德里安去通知卡诺
		vclose();
		PlayMidi("midi\\p1010.mid");
		m_info_st=9;
		m_oldinfo_st=9;
		OpenMap("map\\maps103.bmp","map\\map109c.map","map\\map109d.npc");
		current.x=100;
		current.y=20;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		azimuth= AZIMUTH_DOWN;
		movest=0;
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		blt(384,288,32,48,96,96,0,96,&BuffDC);
		vopen();
		Dialog(4,16,0,0,39);
		st_dialog=71;
	}
	else if(st_dialog==71){
		DrawActor(0);
		Dialog(4,0,31,0,40);
		st_dialog=72;
	}
	else if(st_dialog==72){
		Dialog(4,16,0,0,41);
		st_dialog=73;
	}
	else if(st_dialog==73){
		Dialog(4,0,31,0,42);
		st_dialog=74;
	}
	else if(st_dialog==74){
		Dialog(4,16,0,0,43);
		st_dialog=75;
	}
	else if(st_dialog==75){
		Dialog(4,0,31,0,44);
		st_dialog=76;
	}
	else if(st_dialog==76){
		Dialog(4,16,0,0,45);
		st_dialog=77;
	}
	else if(st_dialog==77){
		Dialog(4,0,31,0,46);
		st_dialog=78;
	}
	else if(st_dialog==78){
		Dialog(4,0,31,0,47);
		st_dialog=79;
	}
	else if(st_dialog==79){
		Dialog(4,16,0,0,48);
		st_dialog=80;
	}
	else if(st_dialog==80){//德里安走
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<2;i++){//移动
			blt(384,(288+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(384,(296+(i*16)),32,48,128,0,32,0,pdc);
			GameSleep(50);
		}
		for(i=0;i<28;i++){//移动
			blt((384-(i*16)),328,32,48,160,48,64,48,pdc);
			GameSleep(50);
			blt((376-(i*16)),328,32,48,128,48,32,48,pdc);
			GameSleep(50);
		}
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=187;///////////////
		PlayMidi("midi\\p1014.mid");
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		SetActTimer();
		st_dialog=2000;
	}
}//st_sub1=1的部分结束
else if(st_sub1==2){
	if(st_dialog==0){
		Dialog(3,22,0,0,50);
		st_dialog=50;
	}
	else if(st_dialog==50){
		Dialog(4,0,46,0,51);
		st_dialog=51;
	}
	else if(st_dialog==51){
		Dialog(4,22,0,0,52);
		st_dialog=52;
	}
	else if(st_dialog==52){//恐怖分子离开
		CDC *pdc=GetDC();
		for(i=20;i<60;i++){
			oldlx=a[1];oldly=a[2];oldx=32;oldy=48;
			MemDC.SelectObject(hbmp_1);//荻娜
			blt(96,(480-(i*16)),32,48,160,96,64,96,pdc);
			a[1]=oldlx;a[2]=oldly;
			//
			oldlx=a[3];oldly=a[4];oldx=32;oldy=32;
			MemDC.SelectObject(hbmp_4);//阿萨德
			blt(96,(544-(i*16)),32,32,64,64,0,64,pdc);
			a[3]=oldlx;a[4]=oldly;
			//
			MemDC.SelectObject(hbmp_6);//恐怖分子
			oldlx=a[5];oldly=a[6];oldx=32;oldy=32;
			blt(64,(608-(i*16)),32,32,0,96,0,0,pdc);
			a[5]=oldlx;a[6]=oldly;
			oldlx=a[7];oldly=a[8];oldx=32;oldy=32;
			blt(64,(640-(i*16)),32,32,0,96,0,0,pdc);
			a[7]=oldlx;a[8]=oldly;
			oldlx=a[9];oldly=a[10];oldx=32;oldy=32;
			blt(64,(672-(i*16)),32,32,0,96,0,0,pdc);
			a[9]=oldlx;a[10]=oldly;
			oldlx=a[11];oldly=a[12];oldx=32;oldy=32;
			blt(64,(704-(i*16)),32,32,0,96,0,0,pdc);
			a[11]=oldlx;a[12]=oldly;
			//
			oldlx=a[13];oldly=a[14];oldx=32;oldy=32;
			blt(128,(608-(i*16)),32,32,64,96,64,0,pdc);
			a[13]=oldlx;a[14]=oldly;
			oldlx=a[15];oldly=a[16];oldx=32;oldy=32;
			blt(128,(640-(i*16)),32,32,64,96,64,0,pdc);
			a[15]=oldlx;a[16]=oldly;
			oldlx=a[17];oldly=a[18];oldx=32;oldy=32;
			blt(128,(672-(i*16)),32,32,64,96,64,0,pdc);
			a[17]=oldlx;a[18]=oldly;
			oldlx=a[19];oldly=a[20];oldx=32;oldy=32;
			blt(128,(704-(i*16)),32,32,64,96,64,0,pdc);
			a[19]=oldlx;a[20]=oldly;
			GameSleep(100);
			////////////////////////
			oldlx=a[1];oldly=a[2];oldx=32;oldy=48;
			MemDC.SelectObject(hbmp_1);//荻娜
			blt(96,(472-(i*16)),32,48,128,96,32,96,pdc);
			a[1]=oldlx;a[2]=oldly;
			//
			oldlx=a[3];oldly=a[4];oldx=32;oldy=32;
			MemDC.SelectObject(hbmp_4);//阿萨德
			blt(96,(544-(i*16)),32,32,96,64,32,64,pdc);
			a[3]=oldlx;a[4]=oldly;
			//
			MemDC.SelectObject(hbmp_6);//恐怖分子
			oldlx=a[5];oldly=a[6];oldx=32;oldy=32;
			blt(64,(608-(i*16)),32,32,96,96,96,0,pdc);
			a[5]=oldlx;a[6]=oldly;
			oldlx=a[7];oldly=a[8];oldx=32;oldy=32;
			blt(64,(640-(i*16)),32,32,96,96,96,0,pdc);
			a[7]=oldlx;a[8]=oldly;
			oldlx=a[9];oldly=a[10];oldx=32;oldy=32;
			blt(64,(672-(i*16)),32,32,96,96,96,0,pdc);
			a[9]=oldlx;a[10]=oldly;
			oldlx=a[11];oldly=a[12];oldx=32;oldy=32;
			blt(64,(704-(i*16)),32,32,96,96,96,0,pdc);
			a[11]=oldlx;a[12]=oldly;
			//
			oldlx=a[13];oldly=a[14];oldx=32;oldy=32;
			blt(128,(608-(i*16)),32,32,160,96,160,0,pdc);
			a[13]=oldlx;a[14]=oldly;
			oldlx=a[15];oldly=a[16];oldx=32;oldy=32;
			blt(128,(640-(i*16)),32,32,160,96,160,0,pdc);
			a[15]=oldlx;a[16]=oldly;
			oldlx=a[17];oldly=a[18];oldx=32;oldy=32;
			blt(128,(672-(i*16)),32,32,160,96,160,0,pdc);
			a[17]=oldlx;a[18]=oldly;
			oldlx=a[19];oldly=a[20];oldx=32;oldy=32;
			blt(128,(704-(i*16)),32,32,160,96,160,0,pdc);
			a[19]=oldlx;a[20]=oldly;
			GameSleep(100);
		}
		current.x+=3;
		current.y+=2;
		azimuth=6;
		ActProc();
		GameSleep(40);
		ActProc();
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,53);
		st_dialog=53;
	}
	else if(st_dialog==53){//卡诺走出
		PlayMidi("midi\\p1010.mid");
		for(i=0;i<24;i++){
			azimuth=7;
			ActProc();
			GameSleep(40);
		}
		Dialog(4,0,31,0,54);
		st_dialog=21;
	}
	else if(st_dialog==60){
		lclose();
		PlayMidi("midi\\p1014.mid");
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map111a.map","map\\map111a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=89;///////////////
		current.x=153;
		current.y=182;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
		st_dialog=2000;
	}
	else if(st_dialog==70){//开始德里安与沙子莫隆司徒等人对话的段落
		PlayMidi("midi\\p1008.mid");
		m_info_st=9;
		m_oldinfo_st=9;
		//切换地图
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		strcpy(oldfname01,fname01);
		strcpy(oldfname02,fname02);
		strcpy(oldfname03,fname03);//保存当前的地图
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x=157;
		current.y=114;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		current.x=oldcurrent.x;
		current.y=oldcurrent.y;
		strcpy(fname01,oldfname01);
		strcpy(fname02,oldfname02);
		strcpy(fname03,oldfname03);//恢复地图
		OpenMap(fname01,fname02,fname03);//调入以前的地图
		//
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_3);//司徒
		resetblt();
		blt(480,192,32,32,64,96,0,96,&BuffDC);
		MemDC.SelectObject(hbmp_8);//白莲教团
		resetblt();
		blt(256,160,64,96,192,224,0,224,&BuffDC);
		MemDC.SelectObject(hbmp_7);//莫隆
		resetblt();
		blt(384,160,32,32,64,0,0,0,&BuffDC);
		vopen();
		Dialog(4,6,0,0,55);
		st_dialog=71;
	}
	else if(st_dialog==71){
		Dialog(4,12,0,0,56);
		st_dialog=72;
	}
	else if(st_dialog==72){
		Dialog(4,0,4,0,57);
		st_dialog=73;
	}
	else if(st_dialog==73){
		Dialog(4,14,0,0,58);
		st_dialog=74;
	}
	else if(st_dialog==74){
		Dialog(4,13,0,0,59);
		st_dialog=75;
	}
	else if(st_dialog==75){
		Dialog(4,13,0,0,60);
		st_dialog=76;
	}
	else if(st_dialog==76){
		Dialog(4,11,0,0,61);
		st_dialog=77;
	}
	else if(st_dialog==77){
		Dialog(4,6,1,0,62);
		st_dialog=78;
	}
	else if(st_dialog==78){
		Dialog(1,6,1,0,63);
		st_dialog=80;
	}
	else if(st_dialog==80){//德里安出现
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<16;i++){//移动
			blt(384,(480-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(50);
			blt(384,(472-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,16,0,0,64);
		st_dialog=81;
	}
	else if(st_dialog==81){
		Dialog(4,0,5,0,65);
		st_dialog=82;
	}
	else if(st_dialog==82){
		Dialog(4,16,0,0,66);
		st_dialog=83;
	}
	else if(st_dialog==83){
		Dialog(4,16,0,0,67);
		st_dialog=84;
	}
	else if(st_dialog==84){
		Dialog(4,0,5,0,68);
		st_dialog=85;
	}
	else if(st_dialog==85){
		Dialog(4,16,0,0,69);
		st_dialog=86;
	}
	else if(st_dialog==86){
		Dialog(4,8,0,0,70);
		st_dialog=87;
	}
	else if(st_dialog==87){
		Dialog(4,16,0,0,71);
		st_dialog=88;
	}
	else if(st_dialog==88){
		Dialog(4,13,0,0,72);
		st_dialog=89;
	}
	else if(st_dialog==89){
		Dialog(4,16,0,0,73);
		st_dialog=90;
	}
	else if(st_dialog==90){
		Dialog(4,13,0,0,74);
		st_dialog=91;
	}
	else if(st_dialog==91){
		Dialog(4,11,0,0,75);
		st_dialog=92;
	}
	else if(st_dialog==92){
		Dialog(4,0,2,0,76);
		st_dialog=93;
	}
	else if(st_dialog==93){
		Dialog(4,11,0,0,77);
		st_dialog=94;
	}
	else if(st_dialog==94){
		Dialog(3,0,5,0,78);
		st_dialog=95;
	}
	else if(st_dialog==95){
		Dialog(4,6,0,0,79);
		st_dialog=100;
	}
	else if(st_dialog==100){//切换回卡诺的位置
		lclose();
		OpenMap("map\\maps103.bmp","map\\map111a.map","map\\map111a.npc");//调入地图
		current.x=13;
		current.y=27;
		azimuth= AZIMUTH_LEFT;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		lopen();
		Dialog(4,0,31,0,80);
		st_dialog=101;
	}
	else if(st_dialog==101){
		st_dialog=102;
		Dialog(5,0,0,0,24);//参数5表示存盘
	}
	else if(st_dialog==102){
		vclose();
		st=9;//第八段
		st_sub1=0;
		st_dialog=0;
		blood=200;//为最后一战，给玩家加满血。
		Dialog(2,0,0,0,94);
		Para8Init();
	}
}//st_sub1=2的部分结束
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para7Accident(int type)
{
	int i;

if((type==202)||(type==224)){//封闭通往南方的道路
	Dialog(4,0,31,0,22);
	st_dialog=21;
}
else if(type==223){//没有心情欣赏火山
	Dialog(4,0,31,0,89);
	st_dialog=21;
}
else if(type==709){//我觉得越走越远了
	Dialog(4,0,31,0,88);
	st_dialog=21;
}
else if(type==236){//山洞
	Dialog(4,0,31,0,92);
	st_dialog=21;
}
else if(type==706){//墓地
	Dialog(4,0,31,0,93);
	st_dialog=21;
}
else if(st_sub1==0){
	if((type==229)||(type==222)||(type==210)){//禁止返回原地图
		Dialog(4,0,31,0,23);
		st_dialog=21;
	}
	else if((type==232)||(type==230)||(type==210)){//我觉得越走越远了
		Dialog(4,0,31,0,88);
		st_dialog=21;
	}
	else if((type==219)||(type==220)){//村中不宜久留
		Dialog(4,0,31,0,90);
		st_dialog=21;
	}
	else if(type==729){//哈德尔
		Dialog(4,26,0,0,83);
		st_dialog=20;
	}
	else if(type==730){//荻娜床前
		Dialog(4,0,31,0,84);
		st_dialog=20;
	}
	else if(type==732){//军官
		Dialog(4,0,0,0,81);
		st_dialog=20;
	}
	else if(type==717){//武器库
		Dialog(4,0,0,0,82);
		st_dialog=20;
	}
	else if(type==715){//村民
		Dialog(4,0,0,0,85);
		st_dialog=20;
	}
	else if(type==725){//防线
		current.x=123;
		current.y=98;//将卡诺重定位于防线之外
		CPoint point;
		int power=10000;
		CDC *pdc=GetDC();
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		processenemies();
		DrawActor(1);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		point.x=399;point.y=239;
		OnShoot(power,point);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,90);
		st_dialog=21;
	}
	else if(type==723){//监狱的门口
		StopActTimer();
		m_info_st=9;
		m_oldinfo_st=9;
		vclose();
		st_dialog=65;
		Dialog(5,0,0,0,24);//参数5表示存盘
	}
	//以下用于处理地图切换。卡诺去监狱，出村子
	else if((type==217)||(type==218)){//地界q和地界r（地图105上方的道路）对应地界t和地界s
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map109c.map","map\\map109c.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=187;///////////////
		current.y=185;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==221){//地界u（地图104上方的道路）对应地界v
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map108d.map","map\\map108g.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		current.x=180;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.y=53;
		int offset_y=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.y+=offset_y;
			offset_y=(offset_y>0? -(offset_y+1) : -(offset_y-1));//offset_y不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==231){//地界金（地图108左下的道路）对应地界丙
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		current.x=178;
		current.y=143;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==227){//地界甲（地图107左上的道路）对应地界A
		lclose();
		StopActTimer();
		OpenMap("map\\maps106.bmp","map\\map106c.map","map\\map106d.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=0;//设定地图基准点
		base_y=187;///////////////
		current.x=180;
		current.y=40;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		SetActTimer();
	}
}//st_sub1=0的部分结束
else if(st_sub1==1){
	if((type==227)||(type==231)||(type==221)){//禁止返回原地图
		Dialog(4,0,31,0,23);
		st_dialog=21;
	}
	else if((type==232)||(type==230)||(type==723)){//返回中央镇要紧
		Dialog(4,0,31,0,95);
		st_dialog=21;
	}
	else if(type==725){//防线//叹，然后迅速藏入森林中，恐怖分子入。
		StopActTimer();
		ActProc();
		m_info_st=9;
		m_oldinfo_st=9;
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_10);//汗叹倒
		resetblt();
		blt(384,180,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		PlayWave("snd//whoa.wav");
		resetblt();
		blt(384,180,35,33,0,0,140,0,pdc);//叹
		//把脚底下占位的800号事件擦掉
		npc[current.x][current.y]=0;
		GameSleep(1000);
		//卡诺走入丛林
		for(i=0;i<24;i++){
			azimuth=5;
			ActProc();
			GameSleep(40);
		}
		for(i=0;i<6;i++){
			azimuth=8;
			ActProc();
			GameSleep(40);
		}
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		PlayMidi("midi\\p1016.mid");
		//荻娜和恐怖分子走入屏幕
		for(i=0;i<40;i++){
			a[i]=0;
		}
		for(i=0;i<20;i++){
			oldlx=a[1];oldly=a[2];oldx=32;oldy=48;
			MemDC.SelectObject(hbmp_1);//荻娜
			blt(96,(480-(i*16)),32,48,160,96,64,96,pdc);
			a[1]=oldlx;a[2]=oldly;
			//
			oldlx=a[3];oldly=a[4];oldx=32;oldy=32;
			MemDC.SelectObject(hbmp_4);//阿萨德
			blt(96,(544-(i*16)),32,32,64,64,0,64,pdc);
			a[3]=oldlx;a[4]=oldly;
			//
			MemDC.SelectObject(hbmp_6);//恐怖分子
			oldlx=a[5];oldly=a[6];oldx=32;oldy=32;
			blt(64,(608-(i*16)),32,32,0,96,0,0,pdc);
			a[5]=oldlx;a[6]=oldly;
			oldlx=a[7];oldly=a[8];oldx=32;oldy=32;
			blt(64,(640-(i*16)),32,32,0,96,0,0,pdc);
			a[7]=oldlx;a[8]=oldly;
			oldlx=a[9];oldly=a[10];oldx=32;oldy=32;
			blt(64,(672-(i*16)),32,32,0,96,0,0,pdc);
			a[9]=oldlx;a[10]=oldly;
			oldlx=a[11];oldly=a[12];oldx=32;oldy=32;
			blt(64,(704-(i*16)),32,32,0,96,0,0,pdc);
			a[11]=oldlx;a[12]=oldly;
			//
			oldlx=a[13];oldly=a[14];oldx=32;oldy=32;
			blt(128,(608-(i*16)),32,32,64,96,64,0,pdc);
			a[13]=oldlx;a[14]=oldly;
			oldlx=a[15];oldly=a[16];oldx=32;oldy=32;
			blt(128,(640-(i*16)),32,32,64,96,64,0,pdc);
			a[15]=oldlx;a[16]=oldly;
			oldlx=a[17];oldly=a[18];oldx=32;oldy=32;
			blt(128,(672-(i*16)),32,32,64,96,64,0,pdc);
			a[17]=oldlx;a[18]=oldly;
			oldlx=a[19];oldly=a[20];oldx=32;oldy=32;
			blt(128,(704-(i*16)),32,32,64,96,64,0,pdc);
			a[19]=oldlx;a[20]=oldly;
			GameSleep(100);
			////////////////////////
			oldlx=a[1];oldly=a[2];oldx=32;oldy=48;
			MemDC.SelectObject(hbmp_1);//荻娜
			blt(96,(472-(i*16)),32,48,128,96,32,96,pdc);
			a[1]=oldlx;a[2]=oldly;
			//
			oldlx=a[3];oldly=a[4];oldx=32;oldy=32;
			MemDC.SelectObject(hbmp_4);//阿萨德
			blt(96,(544-(i*16)),32,32,96,64,32,64,pdc);
			a[3]=oldlx;a[4]=oldly;
			//
			MemDC.SelectObject(hbmp_6);//恐怖分子
			oldlx=a[5];oldly=a[6];oldx=32;oldy=32;
			blt(64,(608-(i*16)),32,32,96,96,96,0,pdc);
			a[5]=oldlx;a[6]=oldly;
			oldlx=a[7];oldly=a[8];oldx=32;oldy=32;
			blt(64,(640-(i*16)),32,32,96,96,96,0,pdc);
			a[7]=oldlx;a[8]=oldly;
			oldlx=a[9];oldly=a[10];oldx=32;oldy=32;
			blt(64,(672-(i*16)),32,32,96,96,96,0,pdc);
			a[9]=oldlx;a[10]=oldly;
			oldlx=a[11];oldly=a[12];oldx=32;oldy=32;
			blt(64,(704-(i*16)),32,32,96,96,96,0,pdc);
			a[11]=oldlx;a[12]=oldly;
			//
			oldlx=a[13];oldly=a[14];oldx=32;oldy=32;
			blt(128,(608-(i*16)),32,32,160,96,160,0,pdc);
			a[13]=oldlx;a[14]=oldly;
			oldlx=a[15];oldly=a[16];oldx=32;oldy=32;
			blt(128,(640-(i*16)),32,32,160,96,160,0,pdc);
			a[15]=oldlx;a[16]=oldly;
			oldlx=a[17];oldly=a[18];oldx=32;oldy=32;
			blt(128,(672-(i*16)),32,32,160,96,160,0,pdc);
			a[17]=oldlx;a[18]=oldly;
			oldlx=a[19];oldly=a[20];oldx=32;oldy=32;
			blt(128,(704-(i*16)),32,32,160,96,160,0,pdc);
			a[19]=oldlx;a[20]=oldly;
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(3,22,0,0,49);
		st_sub1=2;//进入下一个分段
		st_dialog=0;
	}
	//以下用于处理地图切换。
	else if((type==222)){//地界v（地图222右方的道路）对应地界u
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map109c.map","map\\map109b.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=187;///////////////
		current.x=15;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.y=53;
		int offset_y=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.y+=offset_y;
			offset_y=(offset_y>0? -(offset_y+1) : -(offset_y-1));//offset_y不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==229){//地界丙（地图107右方的道路）对应地界金
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map108d.map","map\\map108g.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		current.x=18;
		current.y=143;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==201){//地界a（地图106右上的道路）对应地界甲
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		current.x=15;
		current.y=40;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
}//st_sub1=1的部分结束
else if(st_sub1==2){
	if(type==725){//不许回村
		Dialog(4,0,31,0,108);
		st_dialog=21;
	}
	else if((type==221)||(type==233)){//封闭地界
		Dialog(4,0,31,0,54);
		st_dialog=21;
	}
	else if(type==232){//进入111地图
		st_dialog=60;
		Dialog(5,0,0,0,24);//参数5表示存盘
	}
	else if(type==731){//111地图中加满生命值的泉
		PlayWave("snd//levelup.wav");
		blood=200;
		CDC *pdc=GetDC();
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(500);
		Dialog(1,0,0,0,97);
		st_dialog=21;
	}
	else if(type==234){//开始德里安与沙子莫隆司徒等人对话的段落
		vclose();
		StopActTimer();
		Dialog(2,0,0,0,96);
		st_dialog=70;
	}
}//st_sub1=2的部分结束
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para7Timer()
{
	processenemies();
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████