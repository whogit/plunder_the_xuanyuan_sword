void CMainFrame::Para4Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para4.dlg");//调入对话
	LoadActor("pic\\hero.bmp");//调入主角
	deleteallbmpobjects();
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic300.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic302.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic301.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic320.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic315.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（1）
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic316.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（2）
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic307.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//剑圣莫洛
	hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic308.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//安妮
	hbmp_Photo9=(HBITMAP)LoadImage(NULL,"pic\\pic303.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//教徒
//在沙子落下之后，需要更换图片
/////////////////////////////////////////////////////////////////////////////
//调入所需道具
	hbmp_Prop0=(HBITMAP)LoadImage(NULL,"pic\\pic200.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//轩辕剑
	hbmp_Prop1=(HBITMAP)LoadImage(NULL,"pic\\pic207.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//短刀
	hbmp_Prop2=(HBITMAP)LoadImage(NULL,"pic\\pic203.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//项链
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\pic107.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic104.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//露丝
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic100.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\pic103.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic104.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//教徒
	hbmp_5=(HBITMAP)LoadImage(NULL,"pic\\hero.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺
	hbmp_6=(HBITMAP)LoadImage(NULL,"pic\\pic114.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//圣剑单图
	hbmp_7=(HBITMAP)LoadImage(NULL,"pic\\pic210.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//汗叹倒
	hbmp_8=(HBITMAP)LoadImage(NULL,"pic\\pic108.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
	hbmp_9=(HBITMAP)LoadImage(NULL,"pic\\pic112.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫洛
	hbmp_10=(HBITMAP)LoadImage(NULL,"pic\\pic113.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//安妮
	hbmp_11=(HBITMAP)LoadImage(NULL,"pic\\pic101.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_12=(HBITMAP)LoadImage(NULL,"pic\\pic102.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//CIA
/////////////////////////////////////////////////////////////////////////////
//引入开始游戏的位置
	if(st_sub1==0){
		m_info_st=9;//对话态
		m_oldinfo_st=9;
	}
/////////////////////////////////////////////////////////////////////////////
}//OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图的语句
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para4Info(int type)
{
	int i;

if(st_sub1==0){
	if(st_dialog==0)
	{
		StopMidiPlay();
		Dialog(4,6,0,0,1);//沙子说的四句话
		st_dialog=1;
	}
	else if(st_dialog==1)
	{
		Dialog(4,6,0,0,2);
		st_dialog=2;
	}
	else if(st_dialog==2)
	{
		Dialog(4,6,0,0,3);
		st_dialog=3;
	}
	else if(st_dialog==3)
	{
		Dialog(4,8,0,0,4);
		st_dialog=4;
	}
	else if(st_dialog==4)
	{	
		lclose();
		PlayMidi("midi\\p1012.mid");
		Dialog(2,0,0,0,5);
		st_dialog=5;
	}
	else if(st_dialog==5)
	{//司徒出现在西山山脚下。
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x=106;
		current.y=100;
		azimuth=0;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		MemDC.SelectObject(hbmp_2);//司徒
		resetblt();
		blt(448,228,32,32,64,128,0,128,&BuffDC);//司徒
		vopen();
		Dialog(2,0,0,0,6);
		st_dialog=6;
	}
	else if(st_dialog==6)
	{
		Dialog(4,0,1,0,7);
		st_dialog=7;
	}
	else if(st_dialog==7)
	{
		Dialog(4,0,4,0,8);
		st_dialog=8;
	}
	else if(st_dialog==8)
	{//德里安走来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_8);//德里安
		resetblt();
		for(i=0;i<16;i++)
		{
			blt(352,(480-(i*16)),32,48,128,96,32,96,pdc);//德里安
			GameSleep(100);
			blt(352,(472-(i*16)),32,48,160,96,64,96,pdc);//德里安
			GameSleep(100);
		}
		blt(352,216,32,48,96,144,0,144,pdc);//德里安转过头
		resetblt();
		MemDC.SelectObject(hbmp_2);//司徒
		blt(448,228,32,32,96,96,32,96,pdc);//司徒转过头
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,1,0,9);
		st_dialog=9;
	}
	else if(st_dialog==9)
	{
		Dialog(4,16,0,0,10);
		st_dialog=10;
	}
	else if(st_dialog==10)
	{
		Dialog(4,0,5,0,11);
		st_dialog=11;
	}
	else if(st_dialog==11)
	{
		Dialog(2,0,0,0,12);
		st_dialog=12;
	}
	else if(st_dialog==12)
	{
		Dialog(4,16,0,0,13);
		st_dialog=13;
	}
	else if(st_dialog==13)
	{
		Dialog(4,0,1,0,14);
		st_dialog=14;
	}
	else if(st_dialog==14)
	{
		Dialog(4,16,0,0,15);
		st_dialog=15;
	}
	else if(st_dialog==15)
	{
		Dialog(3,0,2,0,16);
		st_dialog=16;
	}
	else if(st_dialog==16)
	{
		Dialog(4,0,1,0,17);
		st_dialog=17;
	}
	else if(st_dialog==17)
	{
		Dialog(4,16,0,0,18);
		st_dialog=18;
	}
	else if(st_dialog==18)
	{
		Dialog(4,0,4,0,19);
		st_dialog=19;
	}
	else if(st_dialog==19)
	{
		Dialog(3,16,0,0,20);
		st_dialog=60;
	}
	else if(st_dialog==60)
	{
		Dialog(4,16,0,0,21);
		st_dialog=61;
	}
	else if(st_dialog==61)
	{//德里安带路
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//司徒
		blt(448,228,32,32,64,64,0,64,pdc);//司徒转过头
		MemDC.SelectObject(hbmp_8);//德里安
		resetblt();
		for(i=0;i<18;i++)
		{
			blt(352,(216+(i*16)),32,48,128,0,32,0,pdc);//德里安
			GameSleep(100);
			blt(352,(224+(i*16)),32,48,160,0,64,0,pdc);//德里安
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,2,0,22);
		st_dialog=62;

	}
	else if(st_dialog==62)
	{//西镇，卡诺站在洛桑面前。
		vclose();
		PlayMidi("midi\\p1010.mid");
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106b.npc");//调入地图
		current.x=106;
		current.y=138;
		azimuth= AZIMUTH_UP;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic321.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//洛桑镇长
		vopen();
		Dialog(4,0,31,0,23);
		st_dialog=63;
	}
	else if(st_dialog==63)
	{
		Dialog(4,16,0,0,24);
		st_dialog=64;
	}
	else if(st_dialog==64)
	{
		Dialog(4,0,31,0,25);
		st_dialog=65;
	}
	else if(st_dialog==65)
	{
		Dialog(4,16,0,0,26);
		st_dialog=66;
	}
	else if(st_dialog==66)
	{
		Dialog(4,0,31,0,27);
		st_dialog=67;
	}
	else if(st_dialog==67)
	{
		Dialog(4,16,0,0,28);
		st_dialog=68;
	}
	else if(st_dialog==68)
	{
		Dialog(4,0,31,0,29);
		st_dialog=69;
	}
	else if(st_dialog==69)
	{
		Dialog(4,16,0,0,30);
		st_dialog=70;
	}
	else if(st_dialog==70)
	{
		CDC *pdc=GetDC();
		PlayWave("snd//prop.wav");
		m_info_prop2=8;
		RenewInfo(pdc);//在信息栏中显示物品小图
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(1,0,0,2,31);
		st_dialog=71;
	}
	else if(st_dialog==71)
	{
		Dialog(4,0,32,0,32);
		st_dialog=72;
	}
	else if(st_dialog==72)
	{
		Dialog(4,16,0,0,172);
		st_dialog=73;
	}
	else if(st_dialog==73)
	{//转入自由行走状态
		blood=200;
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106b.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=0;//设定地图基准点
		base_y=187;///////////////
		current.x=106;
		current.y=138;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_UP;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
		st_progress=0;
	}
	else if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}//卡诺跟村长说完话，去墓地。
/////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==100)//墓地的片断
	{
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_0);//荻娜
		blt(383,112,32,48,96,0,0,0,pdc);//荻娜转过头
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,22,0,0,37);
		st_dialog=101;
	}
	else if(st_dialog==101)
	{
		Dialog(3,0,31,0,38);
		st_dialog=102;
	}
	else if(st_dialog==102)
	{
		Dialog(4,0,31,0,39);
		st_dialog=103;
	}
	else if(st_dialog==103)
	{
		Dialog(4,22,0,0,40);
		st_dialog=104;
	}
	else if(st_dialog==104)
	{
		Dialog(4,0,31,0,41);
		st_dialog=105;
	}
	else if(st_dialog==105)
	{//山上，剑圣看见荻娜他们在扫墓
		vclose();
		current.x=138;
		current.y=62;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_9);//剑圣
		resetblt();
		blt(415,217,32,48,96,96,0,96,&BuffDC);//剑圣背影
		MemDC.SelectObject(hbmp_10);//安妮
		resetblt();
		blt(383,217,32,48,96,96,0,96,&BuffDC);//安妮背影
		vopen();
		Dialog(2,0,0,0,43);
		st_dialog=107;
	}
	else if(st_dialog==107)
	{
		Dialog(4,0,36,0,44);
		st_dialog=108;
	}
	else if(st_dialog==108)
	{
		Dialog(4,41,0,0,45);
		st_dialog=109;
	}
	else if(st_dialog==109)
	{
		Dialog(4,0,36,0,46);
		st_dialog=120;
	}
	else if(st_dialog==120)
	{
		Dialog(4,41,0,0,47);
		st_dialog=121;
	}
	else if(st_dialog==121)
	{
		Dialog(4,0,36,0,48);
		st_dialog=122;
	}
	else if(st_dialog==122)
	{
		Dialog(4,41,0,0,49);
		st_dialog=123;
	}
	else if(st_dialog==123)
	{
		Dialog(4,0,36,0,50);
		st_dialog=124;
	}
	else if(st_dialog==124)
	{
		Dialog(4,41,0,0,51);
		st_dialog=125;
	}
	else if(st_dialog==125)
	{
		Dialog(4,0,36,0,52);
		st_dialog=126;
	}
	else if(st_dialog==126)
	{
		Dialog(4,0,36,0,53);
		st_dialog=127;
	}
	else if(st_dialog==127)
	{
		Dialog(4,41,0,0,55);
		st_dialog=130;
	}
	else if(st_dialog==130)
	{//切换回墓地
		vclose();
		current.x=119;
		current.y=40;
		azimuth= AZIMUTH_UP;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		Dialog(2,0,0,0,42);
		st_dialog=131;
	}
	else if(st_dialog==131)
	{//在墓地自由行走
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
		st_progress=1;//表明墓碑还没有看过。
	}
/////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==30)
	{//介绍softboy的墓碑
		Dialog(2,0,0,0,61);
		st_dialog=20;
	}
	else if(st_dialog==33)
	{//卧虫的墓碑
		Dialog(4,0,31,0,174);
		st_dialog=20;
	}
	else if(st_dialog==300)
	{//摘星者的墓碑
		Dialog(2,0,0,0,179);
		st_dialog=301;
	}
	else if(st_dialog==301)
	{//摘星者的墓碑
		Dialog(2,0,0,0,180);
		st_dialog=302;
	}
	else if(st_dialog==302)
	{//摘星者的墓碑
		Dialog(2,0,0,0,181);
		st_dialog=303;
	}
	else if(st_dialog==303)
	{//摘星者的墓碑
		Dialog(2,0,0,0,182);
		st_dialog=20;
	}
/////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==140)
	{//离开墓地的一段对话
		Dialog(4,0,31,0,65);
		st_dialog=141;
	}
	else if(st_dialog==141)
	{
		Dialog(4,22,0,0,66);
		st_dialog=142;
	}
	else if(st_dialog==142)
	{
		Dialog(4,0,32,0,67);
		st_dialog=143;
	}
	else if(st_dialog==143)
	{
		Dialog(2,0,0,0,68);
		st_dialog=144;
	}
	else if(st_dialog==144)
	{//离开墓地后的自由战斗
		blood=200;//满血
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		SetActTimer();
		st_dialog=2000;
	}
	else if(st_dialog==21)
	{//如果是战斗状态，就落入这个陷阱。
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==35)
	{
		vclose();
		st_sub1=1;//进入第二段
		st_dialog=0;
		Dialog(2,0,0,0,175);//“与此同时……”
	}
}//st_sub1==0的部分完成。
/////////////////////////////////////////////////////////////////////////////
else if(st_sub1==1){//第二段，从卡诺和荻娜从离开墓地开始
	if(st_dialog==0)
	{//司徒进入剑圣房间

		m_info_st=9;//对话态
		m_oldinfo_st=9;
		PlayMidi("midi\\p1007.mid");
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107b.npc");//地图
		current.x=155;
		current.y=62;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		vopen();
		MemDC.SelectObject(hbmp_2);//司徒
		resetblt();
		CDC *pdc=GetDC();
		for(i=0;i<14;i++)
		{
			blt(352,(480-(i*16)),32,32,64,128,0,128,pdc);
			GameSleep(100);
			blt(352,(472-(i*16)),32,32,96,128,32,128,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,36,0,0,70);
		st_dialog=1;
	}
	else if(st_dialog==1)
	{
		Dialog(4,0,1,0,71);
		st_dialog=2;
	}
	else if(st_dialog==2)
	{
		Dialog(4,36,0,0,72);
		st_dialog=3;
	}
	else if(st_dialog==3)
	{
		Dialog(4,0,2,0,73);
		st_dialog=4;
	}
	else if(st_dialog==4)
	{
		Dialog(4,36,0,0,74);
		st_dialog=5;
	}
	else if(st_dialog==5)
	{
		Dialog(4,0,3,0,75);
		st_dialog=6;
	}
	else if(st_dialog==6)
	{
		Dialog(4,36,0,0,76);
		st_dialog=7;
	}
	else if(st_dialog==7)
	{
		Dialog(2,0,0,0,77);
		st_dialog=8;
	}
	else if(st_dialog==8)
	{
		Dialog(2,0,0,3,78);
		st_dialog=9;
	}
	else if(st_dialog==9)
	{
		Dialog(4,41,5,0,79);
		st_dialog=10;
	}
	else if(st_dialog==10)
	{
		Dialog(4,36,0,0,80);
		st_dialog=11;
	}
	else if(st_dialog==11)
	{//司徒倒
		CDC *pdc=GetDC();
		BuffDC.BitBlt(352,260,96,64,&MapDC,352,260,SRCCOPY);
		pdc->BitBlt(352,260,96,64,&BuffDC,352,260,SRCCOPY);
		MemDC.SelectObject(hbmp_2);//司徒
		blt(352,272,64,32,64,32,0,32,pdc);
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(400,260,35,33,0,0,175,0,pdc);//倒
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,3,0,81);
		st_dialog=12;
	}
	else if(st_dialog==12)
	{//VIA进入
		PlayMidi("midi\\p1002.mid");
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_12);//CIA
		resetblt();
		for(i=0;i<6;i++){//移动第一个CIA
			blt(290,(478-(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(290,(462-(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		BuffDC.BitBlt(352,260,96,64,&MapDC,352,260,SRCCOPY);
		pdc->BitBlt(352,260,96,64,&BuffDC,352,260,SRCCOPY);//擦去司徒
		MemDC.SelectObject(hbmp_2);//司徒站在旁边，举手投降
		oldlx=386;oldly=216;oldx=32;oldy=32;//涂抹老图像
		blt(272,216,64,32,64,0,0,0,pdc);
		MemDC.SelectObject(hbmp_12);//CIA
		resetblt();
		for(i=0;i<5;i++){//移动第二个CIA
			blt(350,(478-(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(350,(462-(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		resetblt();
		MemDC.SelectObject(hbmp_11);//fbi_chief
		resetblt();
		for(i=0;i<12;i++){//移动FBI头目
			blt(384,(478-(i*16)),32,32,64,64,0,64,pdc);
			GameSleep(100);
			blt(384,(470-(i*16)),32,32,96,64,32,64,pdc);
			GameSleep(100);
		}
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,11,0,0,82);
		st_dialog=13;
	}
	else if(st_dialog==13)
	{
		Dialog(4,0,3,0,83);
		st_dialog=14;
	}
	else if(st_dialog==14)
	{
		Dialog(4,11,0,0,84);
		st_dialog=15;
	}
	else if(st_dialog==15)
	{
		Dialog(4,11,0,0,85);
		st_dialog=16;
	}
	else if(st_dialog==16)
	{
		Dialog(4,36,0,0,86);
		st_dialog=17;
	}
	else if(st_dialog==17)
	{
		Dialog(4,41,0,0,87);
		st_dialog=18;
	}
	else if(st_dialog==18)
	{
		Dialog(4,11,0,0,88);
		st_dialog=19;
	}
	else if(st_dialog==19)
	{
		Dialog(4,0,3,0,89);
		st_dialog=50;
	}
	else if(st_dialog==50)
	{
		Dialog(4,13,0,0,90);
		st_dialog=51;
	}
	else if(st_dialog==51)
	{
		Dialog(4,0,0,0,91);
		st_dialog=52;
	}
	else if(st_dialog==52)
	{
		Dialog(4,0,3,0,92);
		st_dialog=53;
	}
	else if(st_dialog==53)
	{
		Dialog(4,11,0,0,93);
		st_dialog=54;
	}
	else if(st_dialog==54)
	{//白莲教进入
		PlayMidi("midi\\p1011.mid");
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_4);//白莲教教徒
		resetblt();
		for(i=0;i<5;i++){//移动第一个教徒
			blt(290,(478-(i*32)),32,32,96,64,32,64,pdc);
			GameSleep(100);
			blt(290,(462-(i*32)),32,32,64,64,0,64,pdc);
			GameSleep(100);
		}
		resetblt();
		for(i=0;i<4;i++){//移动第二个教徒
			blt(350,(478-(i*32)),32,32,96,64,32,64,pdc);
			GameSleep(100);
			blt(350,(462-(i*32)),32,32,64,64,0,64,pdc);
			GameSleep(100);
		}
		resetblt();
		for(i=0;i<5;i++){//移动第三个教徒
			blt(416,(478-(i*32)),32,32,96,64,32,64,pdc);
			GameSleep(100);
			blt(416,(462-(i*32)),32,32,64,64,0,64,pdc);
			GameSleep(100);
		}
		blt(416,292,32,32,64,32,0,32,pdc);
		resetblt();
		MemDC.SelectObject(hbmp_3);//沙子
		resetblt();
		for(i=0;i<8;i++){//移动沙子
			blt(384,(478-(i*16)),32,32,64,64,0,64,pdc);
			GameSleep(100);
			blt(384,(470-(i*16)),32,32,96,64,32,64,pdc);
			GameSleep(100);
		}
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,94);
		st_dialog=55;
	}
	else if(st_dialog==55)
	{
		Dialog(4,46,46,0,95);
		st_dialog=56;
	}
	else if(st_dialog==56)
	{
		Dialog(4,0,6,0,96);
		st_dialog=57;
	}
	else if(st_dialog==57)
	{//CIA调转枪口
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_12);//CIA
		oldlx=290;oldly=302;oldx=32;oldy=32;//涂抹老图像
		blt(290,302,32,32,32,160,32,64,pdc);
		oldlx=350;oldly=334;oldx=32;oldy=32;//涂抹老图像
		blt(350,334,32,32,32,160,32,64,pdc);
		MemDC.SelectObject(hbmp_11);//莫隆
		oldlx=384;oldly=296;oldx=32;oldy=32;//涂抹老图像
		blt(384,296,32,32,96,0,32,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,13,0,0,97);
		st_dialog=58;
	}
	else if(st_dialog==58)
	{//沙子夺取轩辕剑
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_6);//圣剑发光的图片
		PlayWave("snd//prop.wav");
		resetblt();
		blt(352,152,32,48,96,48,0,48,pdc);
		GameSleep(200);
		blt(352,152,32,48,128,48,32,48,pdc);
		GameSleep(200);
		blt(352,152,32,48,160,48,64,48,pdc);
		GameSleep(200);
		blt(352,152,32,48,128,48,32,48,pdc);
		GameSleep(200);
		blt(352,152,32,48,96,48,0,48,pdc);
		GameSleep(200);
		//转移到沙子手中
		PlayWave("snd//prop.wav");
		blt(384,342,32,48,96,48,0,48,pdc);
		GameSleep(200);
		blt(384,342,32,48,128,48,32,48,pdc);
		GameSleep(200);
		blt(384,342,32,48,160,48,64,48,pdc);
		GameSleep(200);
		blt(384,342,32,48,128,48,32,48,pdc);
		GameSleep(200);
		blt(384,342,32,48,96,48,0,48,pdc);
		GameSleep(200);
		blt(0,0,0,0,0,0,0,0,pdc);
		MemDC.SelectObject(hbmp_3);//沙子转过头来
		oldlx=384;oldly=342;oldx=32;oldy=32;//涂抹老图像
		blt(384,358,32,32,64,0,0,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(4,0,9,0,98);
		st_dialog=59;
	}
	else if(st_dialog==59)
	{//沙子走，然后所有人汗，叹。
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_3);//沙子
		for(i=0;i<9;i++){//移动沙子
			blt(384,(358+(i*16)),32,32,64,0,0,0,pdc);
			GameSleep(100);
			blt(384,(366+(i*16)),32,32,96,0,32,0,pdc);
			GameSleep(100);
		}
		GameSleep(1000);
		//大家出汗
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(286,184,35,33,0,0,35,0,pdc);//汗
		resetblt();
		blt(360,176,35,33,0,0,35,0,pdc);//汗
		resetblt();
		blt(392,176,35,33,0,0,35,0,pdc);//汗
		resetblt();
		blt(296,256,35,33,0,0,35,0,pdc);//汗
		resetblt();
		blt(360,288,35,33,0,0,35,0,pdc);//汗
		resetblt();
		blt(392,256,35,33,0,0,35,0,pdc);//汗
		GameSleep(1000);
		resetblt();
		blt(286,184,35,33,0,0,70,0,pdc);//汗
		resetblt();
		blt(360,176,35,33,0,0,70,0,pdc);//汗
		resetblt();
		blt(392,176,35,33,0,0,70,0,pdc);//汗
		resetblt();
		blt(296,256,35,33,0,0,70,0,pdc);//汗
		resetblt();
		blt(360,288,35,33,0,0,70,0,pdc);//汗
		resetblt();
		blt(392,256,35,33,0,0,70,0,pdc);//汗
		GameSleep(1000);
		resetblt();
		blt(286,184,35,33,0,0,140,0,pdc);//叹
		resetblt();
		blt(360,176,35,33,0,0,140,0,pdc);//叹
		resetblt();
		blt(392,176,35,33,0,0,140,0,pdc);//叹
		resetblt();
		blt(296,256,35,33,0,0,140,0,pdc);//叹
		resetblt();
		blt(360,288,35,33,0,0,140,0,pdc);//叹
		resetblt();
		blt(392,256,35,33,0,0,140,0,pdc);//叹
		GameSleep(2000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(1,14,3,0,99);
		st_dialog=60;
	}
	else if(st_dialog==60)
	{
		Dialog(1,13,4,0,100);
		st_dialog=61;
	}
	else if(st_dialog==61)
	{//外景，沙子高举轩辕剑……
		lclose();
		StopMidiPlay();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107b.npc");//地图
		current.x=155;
		current.y=77;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		//在地图上绘制CIA的人
		MemDC.SelectObject(hbmp_12);//CIA
		resetblt();
		blt(290,70,32,32,32,160,32,64,&BuffDC);
		resetblt();
		blt(350,102,32,32,32,160,32,64,&BuffDC);
		MemDC.SelectObject(hbmp_11);//莫隆
		resetblt();
		blt(384,64,32,32,96,0,32,0,&BuffDC);
		//绘制白莲教的人
		hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic110.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		MemDC.SelectObject(hbmp_4);
		resetblt();
		blt(310,270,96,64,192,64,0,64,&BuffDC);
		//绘制圣剑
		MemDC.SelectObject(hbmp_6);//圣剑发光的图片
		resetblt();
		blt(352,224,32,48,96,48,0,48,&BuffDC);
		lopen();
		Dialog(4,0,9,0,101);
		st_dialog=62;
	}
	else if(st_dialog==62)
	{
		MemDC.SelectObject(hbmp_6);//轩辕剑一闪
		PlayWave("snd//prop.wav");
		CDC *pdc=GetDC();
		resetblt();
		blt(352,224,32,48,96,48,0,48,pdc);
		GameSleep(200);
		blt(352,224,32,48,128,48,32,48,pdc);
		GameSleep(200);
		blt(352,224,32,48,160,48,64,48,pdc);
		GameSleep(200);
		blt(352,224,32,48,128,48,32,48,pdc);
		GameSleep(200);
		blt(352,224,32,48,96,48,0,48,pdc);
		GameSleep(200);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,8,0,102);
		st_dialog=63;
	}
	else if(st_dialog==63)
	{
		Dialog(3,0,8,0,103);
		st_dialog=64;
	}
	else if(st_dialog==64)
	{
		Dialog(3,0,8,0,104);
		st_dialog=65;
	}
	else if(st_dialog==65)
	{
		Dialog(4,13,0,0,105);
		st_dialog=66;
	}
	else if(st_dialog==66)
	{//VIA射击，沙子落下
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_12);//CIA
		for(i=0;i<3;i++){
			PlayWave("snd//shoot.wav");
			resetblt();
			blt(290,70,32,32,224,160,224,64,pdc);
			resetblt();
			blt(350,102,32,32,224,160,224,64,pdc);
			GameSleep(200);
			oldlx=290;oldly=70;oldx=32;oldy=32;//涂抹老图像
			blt(290,70,32,32,32,160,32,64,pdc);
			oldlx=350;oldly=102;oldx=32;oldy=32;//涂抹老图像
			blt(350,102,32,32,32,160,32,64,pdc);
			GameSleep(200);
		}
		//爆炸
		hbmp_12=(HBITMAP)LoadImage(NULL,"pic\\pic007.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		PlayWave("snd\\bomb2.wav");
		oldlx=352;oldly=224;oldx=32;oldy=48;//
		blt(0,0,0,0,0,0,0,0,pdc);//擦去轩辕剑
		MemDC.SelectObject(hbmp_12);//爆炸图片
		for(i=0;i<10;i++){
			pdc->BitBlt(200,250,400,200,&BuffDC,200,250,SRCCOPY);
			pdc->StretchBlt(300,250,108,100,&MemDC,(i*54),50,54,50,SRCAND);
			pdc->StretchBlt(300,250,108,100,&MemDC,(i*54),0,54,50,SRCINVERT);
//			pdc->BitBlt(200,250,400,200,&BuffDC,200,250,SRCCOPY);
			GameSleep(100);
		}
		BuffDC.BitBlt(200,250,400,200,&MapDC,200,250,SRCCOPY);
		pdc->BitBlt(200,250,400,200,&BuffDC,200,250,SRCCOPY);
		//白莲教教徒散开
		hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic104.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		MemDC.SelectObject(hbmp_4);
		resetblt();
		blt(210,270,32,32,64,64,0,64,pdc);
		resetblt();
		blt(510,270,32,32,64,64,0,64,pdc);
		resetblt();
		blt(258,334,32,32,64,64,0,64,pdc);
		//沙子炸飞，落下
		MemDC.SelectObject(hbmp_3);//沙子
		resetblt();
		int j=270;
		for(i=0;i<12;i++){
			blt(352,(j-=i),32,32,96,128,32,128,pdc);
			GameSleep(50);
		}
		GameSleep(100);
		for(i=0;i<24;i++){
			blt(352,(j+=i),32,32,96,128,32,128,pdc);
			GameSleep(50);
		}
		PlayWave("snd//mdie.wav");
		GameSleep(3000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,106);
		st_dialog=70;
	}
	else if(st_dialog==70)
	{//卡诺把荻娜推开
		lclose();
		current.x=175;
		current.y=134;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		//绘制卡诺和荻娜，卡诺在前，荻娜在后。
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		blt(352,368,32,48,96,144,0,144,&BuffDC);
		MemDC.SelectObject(hbmp_5);//卡诺
		resetblt();
		blt(320,368,32,48,96,144,0,144,&BuffDC);
		lopen();
		CDC *pdc=GetDC();
		//叹号
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(318,330,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		PlayWave("snd//whoa.wav");
		resetblt();
		blt(318,330,35,33,0,0,140,0,pdc);//叹
		GameSleep(1000);
		//推开荻娜
		MemDC.SelectObject(hbmp_5);//卡诺
		oldlx=320;oldly=368;oldx=32;oldy=48;//涂抹老图像
		PlayWave("system//enemies//wound.wav");
		blt(320,368,48,48,144,192,48,192,pdc);
		GameSleep(200);
		blt(320,368,32,48,96,144,0,144,pdc);
		MemDC.SelectObject(hbmp_0);//荻娜
		oldlx=352;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(384,368,32,48,96,192,0,192,pdc);
		PlayWave("system//enemies//fire.wav");
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,0,0,107);
		st_dialog=71;
	}
	else if(st_dialog==71)
	{
		hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic311.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（2）
		MemDC.SelectObject(hbmp_3);//沙子
		CDC *pdc=GetDC();
		resetblt();
		for(i=0;i<25;i++){
			blt(352,(i*16),32,32,96,128,32,128,pdc);
			GameSleep(50);
		}
		PlayWave("snd//open.wav");
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(4,0,36,0,108);
		st_dialog=72;
	}
	else if(st_dialog==72)
	{
		GameSleep(500);
		CDC *pdc=GetDC();
		oldlx=318;oldly=330;oldx=35;oldy=33;//涂抹老图像
		blt(0,0,0,0,0,0,0,0,pdc);
		MemDC.SelectObject(hbmp_0);//荻娜
		oldlx=384;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(384,368,32,48,96,48,0,48,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(4,22,0,0,109);
		st_dialog=73;
	}
	else if(st_dialog==73)
	{
		Dialog(4,0,31,0,110);
		st_dialog=74;
	}
	else if(st_dialog==74)
	{
		Dialog(4,21,0,0,111);
		st_dialog=75;
	}
	else if(st_dialog==75)
	{
		Dialog(4,0,31,0,112);
		st_dialog=76;
	}
	else if(st_dialog==76)
	{
		Dialog(4,22,0,0,113);
		st_dialog=77;
	}
	else if(st_dialog==77)
	{
		GameSleep(500);
		Dialog(4,0,6,0,114);
		st_dialog=78;
	}
	else if(st_dialog==78)
	{//沙子站起来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_3);//沙子
		oldlx=352;oldly=384;oldx=32;oldy=32;//涂抹老图像
		blt(352,384,32,32,96,0,32,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(500);
		Dialog(4,0,6,0,115);
		st_dialog=79;
	}
	else if(st_dialog==79)
	{
		Dialog(4,22,0,0,116);
		st_dialog=80;
	}
	else if(st_dialog==80)
	{//沙子走
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_5);//卡诺
		oldlx=320;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(320,368,32,48,96,0,0,0,pdc);
		MemDC.SelectObject(hbmp_0);//荻娜
		oldlx=384;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(384,368,32,48,96,0,0,0,pdc);
		MemDC.SelectObject(hbmp_3);//沙子
		resetblt();
		for(i=0;i<8;i++){//移动沙子
			blt(352,(384+(i*16)),32,32,64,0,0,0,pdc);
			GameSleep(100);
			blt(352,(392+(i*16)),32,32,96,0,32,0,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,6,0,117);
		st_dialog=81;
	}
	else if(st_dialog==81)
	{
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_5);//卡诺
		oldlx=320;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(320,368,32,48,96,0,0,0,pdc);
		MemDC.SelectObject(hbmp_0);//荻娜
		oldlx=384;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(384,368,32,48,96,0,0,0,pdc);
		GameSleep(500);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,26,0,0,118);
		st_dialog=82;
	}
	else if(st_dialog==82)
	{
		Dialog(4,0,32,0,119);
		st_dialog=83;
	}
	else if(st_dialog==83)
	{//圣剑落下
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_6);
		resetblt();
		for(i=0;i<24;i++){
			blt(352,(i*16),32,48,96,0,0,0,pdc);
			GameSleep(50);
		}
		PlayWave("snd//open.wav");
		MemDC.SelectObject(hbmp_5);//卡诺
		oldlx=320;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(320,368,32,48,96,144,0,144,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(4,0,36,0,120);
		st_dialog=84;
	}
	else if(st_dialog==84)
	{//圣剑闪光
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_0);//荻娜
		oldlx=384;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(384,368,32,48,96,48,0,48,pdc);
		MemDC.SelectObject(hbmp_6);
		PlayWave("snd//prop.wav");
		resetblt();
		blt(352,368,32,48,96,0,0,0,pdc);
		GameSleep(200);
		blt(352,368,32,48,128,0,32,0,pdc);
		GameSleep(200);
		blt(352,368,32,48,160,0,64,0,pdc);
		GameSleep(200);
		blt(352,368,32,48,128,0,32,0,pdc);
		GameSleep(200);
		blt(352,368,32,48,96,0,0,0,pdc);
		GameSleep(200);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(4,24,0,0,121);
		st_dialog=85;
	}
	else if(st_dialog==85)
	{
		Dialog(4,0,32,0,122);
		st_dialog=86;
	}
	else if(st_dialog==86)
	{
		Dialog(4,0,32,0,123);
		st_dialog=87;
	}
	else if(st_dialog==87)
	{//得到剑
		CDC *pdc=GetDC();
		oldlx=352;oldly=368;oldx=32;oldy=48;//涂抹老图像
		blt(0,0,0,0,0,0,0,0,pdc);
		PlayWave("snd//prop.wav");
		m_info_prop3=1;
		RenewInfo(pdc);//在信息栏中显示物品小图
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,1,124);
		st_dialog=88;
	}
	else if(st_dialog==88){
		st_dialog=89;
		Dialog(5,0,0,0,69);//提示存盘
	}
	else if(st_dialog==89)
	{
		lclose();
		PlayMidi("midi\\p1014.mid");
		CDC *pdc=GetDC();
		OpenMap("map\\maps103.bmp","map\\map108b.map","map\\map108e.npc");//调入地图的语句
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		current.x=17;
		current.y=140;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_DOWN;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		//更换部分头像图片
		hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic320.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
		hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic311.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（2）
		hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic323.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡洛德
		hbmp_Photo9=(HBITMAP)LoadImage(NULL,"pic\\pic312.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（3）
		lopen();
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,42);
		st_sub1=2;
		st_dialog=20;
	}
}//st_sub1==1的部分完成。
//////////////////////////////////////////////////////////////////////////////
else if(st_sub1==2){//第三段，从卡诺捡到落下的轩辕剑开始
	if(st_dialog==20)
	{//自有走动时，对话的陷入段。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==21)
	{//自有战斗时，对话的陷入段。
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==30)
	{
		Dialog(4,21,0,0,126);
		st_dialog=31;
	}
	else if(st_dialog==31)
	{
		Dialog(4,0,41,0,127);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{
		Dialog(4,0,41,0,128);
		st_dialog=33;
	}
	else if(st_dialog==33)
	{
		Dialog(4,22,0,0,129);
		st_dialog=20;
	}//跟卡洛德的对话结束
	else if(st_dialog==40)
	{
		Dialog(4,21,0,0,131);
		st_dialog=41;
	}
	else if(st_dialog==41)
	{
		Dialog(4,0,16,0,132);
		st_dialog=42;
	}
	else if(st_dialog==42)
	{
		Dialog(4,21,0,0,133);
		st_dialog=43;
	}
	else if(st_dialog==43)
	{
		Dialog(4,0,16,0,134);
		st_dialog=44;
	}
	else if(st_dialog==44)
	{
		Dialog(4,0,46,0,135);
		st_dialog=45;
	}
	else if(st_dialog==45)
	{
		Dialog(4,0,16,0,136);
		st_dialog=46;
	}
	else if(st_dialog==46)
	{//卡诺发现卡洛德走来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_5);//卡诺
		oldlx=384;oldly=208;oldx=32;oldy=48;//涂抹老图像
		blt(384,208,32,48,96,48,0,48,pdc);
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(384,170,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		PlayWave("snd//whoa.wav");
		blt(384,170,35,33,0,0,140,0,pdc);//叹
		GameSleep(1000);
		MemDC.SelectObject(hbmp_5);//卡诺
		oldlx=384;oldly=208;oldx=32;oldy=48;//涂抹老图像
		blt(384,208,32,48,96,144,0,144,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,36,0,138);
		st_dialog=47;
	}
	else if(st_dialog==47)
	{
		Dialog(4,0,16,0,139);
		st_dialog=50;
	}
	else if(st_dialog==50)
	{//躲入树后
		lclose();
		OpenMap("map\\maps103.bmp","map\\map108a.map","map\\map108e.npc");
		current.x=96;
		current.y=159;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_5);//卡诺
		resetblt();
		blt(352,208,32,24,96,0,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_8);//老头
		resetblt();
		blt(384,208,32,24,96,0,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		blt(416,208,32,24,96,0,0,0,&BuffDC);
		lopen();
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_8);
		resetblt();
		for(i=0;i<51;i++){
			blt((i*16),336,32,48,128,144,32,144,pdc);
			GameSleep(50);
			blt((8+(i*16)),336,32,48,160,144,64,144,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,16,0,177);
		st_dialog=51;
	}
	else if(st_dialog==51)
	{//德里安看山洞
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_8);
		resetblt();
		for(i=0;i<6;i++){
			blt(384,(184-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(100);
			blt(384,(176-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,16,0,140);
		st_dialog=52;
	}
	else if(st_dialog==52)
	{//卡诺和荻娜也走过去
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_5);
		resetblt();
		for(i=0;i<6;i++){
			blt(352,(184-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(100);
			blt(352,(176-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(100);
		}
		MemDC.SelectObject(hbmp_0);
		resetblt();
		for(i=0;i<6;i++){
			blt(416,(184-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(100);
			blt(416,(176-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,46,0,141);
		st_dialog=53;
	}
	else if(st_dialog==53)
	{
		Dialog(4,0,16,0,142);
		st_dialog=54;
	}
	else if(st_dialog==54)
	{
		lclose();
		PlayMidi("midi\\p1017.mid");
		CDC *pdc=GetDC();
		OpenMap("map\\maps102.bmp","map\\map102a.map","map\\map102a.npc");//调入地图的语句
		m_info_map=0;//关闭地图支持
		m_oldinfo_map=0;///////////
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		current.x=104;
		current.y=185;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_UP;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		lopen();
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,143);
		st_dialog=21;
	}
	else if(st_dialog==60)
	{//发现古代文字的对话
		Dialog(4,0,46,0,146);
		st_dialog=61;
	}
	else if(st_dialog==61)
	{
		Dialog(4,0,16,0,147);
		st_dialog=62;
	}
	else if(st_dialog==62)
	{
		Dialog(4,0,16,0,148);
		st_dialog=63;
	}
	else if(st_dialog==63)
	{
		Dialog(4,21,31,0,149);
		st_dialog=64;
	}
	else if(st_dialog==64)
	{
		Dialog(4,0,16,0,150);
		st_dialog=65;
	}
	else if(st_dialog==65)
	{
		Dialog(4,0,16,0,151);
		st_dialog=66;
	}
	else if(st_dialog==66)
	{
		Dialog(4,0,16,0,152);
		st_dialog=67;
	}
	else if(st_dialog==67)
	{
		Dialog(4,0,16,0,153);
		st_dialog=68;
	}
	else if(st_dialog==68)
	{
		Dialog(4,0,16,0,154);
		st_dialog=69;
	}
	else if(st_dialog==69)
	{
		Dialog(4,0,16,0,155);
		st_dialog=70;
	}
	else if(st_dialog==70)
	{
		Dialog(4,0,16,0,156);
		st_dialog=71;
	}
	else if(st_dialog==71)
	{
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_8);//老头
		resetblt();
		oldlx=384;oldly=208;oldx=32;oldy=48;//涂抹老图像
		blt(384,208,32,48,96,48,0,48,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,16,0,157);
		st_dialog=72;
	}
	else if(st_dialog==72)
	{
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_5);//卡诺
		resetblt();
		oldlx=352;oldly=208;oldx=32;oldy=48;//涂抹老图像
		blt(352,208,32,48,96,144,0,144,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,158);
		st_dialog=73;
	}
	else if(st_dialog==73)
	{
		Dialog(4,0,16,0,159);
		st_dialog=74;
	}
	else if(st_dialog==74)
	{
		Dialog(4,0,46,0,160);
		st_dialog=75;
	}
	else if(st_dialog==75)
	{
		Dialog(4,0,16,0,161);
		st_dialog=76;
	}
	else if(st_dialog==76)
	{
		Dialog(4,0,46,0,162);
		st_dialog=77;
	}
	else if(st_dialog==77)
	{
		Dialog(4,0,16,0,163);
		st_dialog=78;
	}
	else if(st_dialog==78)
	{
		Dialog(4,0,16,0,164);
		st_dialog=79;
	}
	else if(st_dialog==79)
	{
		Dialog(4,0,36,0,165);
		st_dialog=80;
	}
	else if(st_dialog==80)
	{
		Dialog(4,0,16,0,166);
		st_dialog=81;
	}
	else if(st_dialog==81)
	{//卡洛德冲进来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_8);
		resetblt();
		for(i=0;i<10;i++){
			blt(384,(480-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(100);
			blt(384,(472-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,41,0,167);
		st_dialog=82;
	}
	else if(st_dialog==82)
	{
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_5);//卡诺
		resetblt();
		blt(352,208,32,48,96,0,0,0,pdc);
		MemDC.SelectObject(hbmp_8);//老头
		resetblt();
		blt(384,208,32,48,96,0,0,0,pdc);
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		blt(416,208,32,48,96,0,0,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,16,0,168);
		st_dialog=83;
	}
	else if(st_dialog==83)
	{
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(384,298,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		PlayWave("snd//whoa.wav");
		blt(384,298,35,33,0,0,140,0,pdc);//叹
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,41,0,169);
		st_dialog=84;
	}
	else if(st_dialog==84)
	{
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_6);//圣剑
		oldlx=384;oldly=298;oldx=35;oldy=33;//涂抹老图像
		blt(352,234,32,48,96,0,0,0,pdc);
		PlayWave("snd//open.wav");
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,32,0,170);
		st_dialog=85;
	}
	else if(st_dialog==85)
	{
		vclose();
		st_dialog=86;
		Dialog(5,0,0,0,69);//参数5表示存盘
	}
	else if(st_dialog==86)
	{
		st=6;//第五段
		st_sub1=0;
		st_dialog=0;
		Dialog(2,0,0,0,171);
		Para5Init();
	}
}//st_sub1==2的部分完成。
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para4Accident(int type)
{
if(st_sub1==0){
	if(type==171){//村民
		Dialog(4,0,0,0,33);
		st_dialog=20;
	}
	else if(type==170){//镇长
		Dialog(4,16,0,0,172);
		st_dialog=20;
	}
	else if(type==168){//不说话的士兵
		Dialog(4,0,0,0,63);
		st_dialog=20;
	}
	else if(type==202){//地界B
		Dialog(4,0,31,0,34);
		st_dialog=20;
	}//西镇的人物结束
	//以下部分是墓地的事件
	else if(type==701){//softboy
		Dialog(2,0,0,0,60);
		st_dialog=30;
	}
	else if(type==702){//司徒键盘
		Dialog(2,0,0,0,62);
		st_dialog=20;
	}
	else if(type==703){//荻娜难友
		Dialog(2,0,0,0,57);
		st_dialog=20;
	}
	else if(type==704){//卧虫
		st_progress=2;
		Dialog(2,0,0,0,58);
		st_dialog=33;
	}
	else if(type==727){//摘星者
		Dialog(2,0,0,0,178);
		st_dialog=300;
	}
	else if(type==705){//其他
		Dialog(2,0,0,0,63);
		st_dialog=20;
	}
	else if(type==707){//禁止返回西镇的地界
		Dialog(4,21,0,0,173);
		st_dialog=21;//存在战斗
	}
	else if(type==229){//地界丙需要根据情况分别处理
		if(st_progress==0){
			Dialog(4,0,31,0,34);
			st_dialog=21;
		}
		else if(st_progress==3){//离开墓地后，沙子在此处落下。
			st_dialog=35;
			Dialog(5,0,0,0,69);//参数5表示存盘
		}
	}
////////////////////////////////////////////////////////////////////////////////
	else if(type==706){//到达墓地（进入下一阶段的入口）
		if(st_progress==0){//如果是第一次来，则进入墓地。
			StopActTimer();//遇见荻娜正在扫墓
			m_info_st=9;//对话态
			m_oldinfo_st=9;
			lclose();
			OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107b.npc");//不能回西镇的地图
			CDC *pdc=GetDC();
			current.x=119;
			current.y=40;
			azimuth= AZIMUTH_UP;
			movest=0;
			DrawMap();//准备地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			DrawActor(1);
			MemDC.SelectObject(hbmp_0);//荻娜
			resetblt();
			blt(383,112,32,48,96,96,0,96,pdc);//荻娜背影
			ReleaseDC(pdc);
			pdc = NULL;
			lopen();
			Dialog(4,0,31,0,36);
			st_dialog=100;
			}
		else if(st_progress==1){//没有去看卧虫的墓碑，不能离开墓地。
			Dialog(4,0,31,0,56);
			st_dialog=20;
		}
		else if(st_progress==2){//离开墓地。
			lclose();
			current.y+=3;
			oldcurrent.y=current.y;
			DrawMap();//准备地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			DrawActor(1);
			lopen();
			Dialog(4,22,0,0,64);
			st_dialog=140;
			st_progress=3;
		}
		else if(st_progress==3){//离开墓地后，又返回墓地。
			Dialog(4,0,31,0,54);
			st_dialog=21;
		}
	}
////////////////////////////////////////////////////////////////////////////////
	//以下是地图的切换段
	else if(type==201){//地界A（地图106右上的道路）对应地界甲
		lclose();
		PlayMidi("midi\\p1014.mid");
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		current.x-=165;
		current.y=40;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4&&bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==227){//地界甲（地图107左上的道路）对应地界A
		lclose();
		PlayMidi("midi\\p1010.mid");
		StopActTimer();
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106b.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=0;//设定地图基准点
		base_y=187;///////////////
		current.x+=165;
		current.y=40;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
	}
}//st_sub1==1的部分完成。
///////////////////////////////////////////////////////////////////////////////
if(st_sub1==2){
	if(type==231){//地界金
		Dialog(4,0,31,0,176);
		st_dialog=20;
	}
	if(type==186){//碰见卡洛德
		Dialog(4,0,41,0,125);
		st_dialog=30;
	}
	if(type==187){//碰见德里安
		Dialog(4,0,16,0,130);
		st_dialog=40;
	}
	if(type==236){//企图走出山洞
		Dialog(4,0,16,0,144);
		st_dialog=21;
	}
	if(type==708){//古代文字的石壁
		PlayMidi("midi\\p1018.mid");
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_5);//卡诺
		resetblt();
		blt(352,208,32,48,96,96,0,96,pdc);
		MemDC.SelectObject(hbmp_8);//老头
		resetblt();
		blt(384,208,32,48,96,96,0,96,pdc);
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		blt(416,208,32,48,96,96,0,96,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,16,0,145);
		m_info_st=9;//对话态
		m_oldinfo_st=9;
		st_dialog=60;
	}
}//st_sub1==2的部分完成。
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para4Timer()
{
	processenemies();
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████