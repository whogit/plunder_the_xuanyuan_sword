// Hero.cpp: implementation of the CHero class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Game.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHero::CHero( CGameMap* pGameMap )
	: CGameObject( pGameMap )
{
	// 死亡动画
	m_hDeadDC  = pGameMap->m_hNpcDeadDC;

	// 默认面下站立
	m_nStatus = 6;
	// 默认动作都已经完成
	m_wAct = 0xC0;
	// 默认尺寸
	m_w = HERO_W;
	m_h = HERO_H;
	// 默认属性
	m_nLife = 1000;
	m_nLevel = 50;
	m_nExp = 0;
	m_hImageDC2 = NULL;
}

CHero::~CHero()
{
	if( m_hImageDC2 != NULL )
	{
		::DeleteDC( m_hImageDC2 );
		m_hImageDC2 = NULL;
	}
}

// 绘制
void CHero::Draw( HDC hDC )
{
	int nLeft = m_pGameMap->m_nLeft;
	int nTop  = m_pGameMap->m_nTop;

	if( (m_x + m_w < nLeft) || (m_x > nLeft + GAME_AREA_W) )
		return;
	if( (m_y + m_w < nTop)  || (m_y > nTop  + GAME_AREA_H) )
		return;

	int x  = m_x - nLeft;
	int y  = m_y - nTop;

	// 绘制死亡动画
	if( m_wAct == HERO_ACT_DEAD )
	{
		TransBlt( hDC, x-(110-NPC_W)/2, y-(60-NPC_H)/2, 110, 60, m_hDeadDC, 0, 60*(m_nStatus-100), COLORKEY );
		m_nStatus ++;
		if( m_nStatus == 109 )
			m_pGameMap->m_pMainFrm->HeroDead();
		return;
	}

	int nIndex = (m_nStatus % 20) - 1;
	int sx = hero_act_x[ nIndex ];
	int sy = hero_act_y[ nIndex ];
	if( m_nStatus > 20 )
		sy += 192;
	if( m_nStatus > 40 )
		sy += 192;

	// 绘制副角色
	if( m_hImageDC2 != NULL )
	{
		// 副角色总是在角色的右上方
		TransBlt( hDC, m_x-nLeft + HERO2_OFFSET_X, m_y-nTop + HERO2_OFFSET_Y, m_w, m_h, m_hImageDC2, sx, sy, COLORKEY );
	}

	// 绘制角色（角色总是在副角色的左下方，会遮挡副角色）
	TransBlt( hDC, m_x-nLeft, m_y-nTop, m_w, m_h, m_hImageDC, sx, sy, COLORKEY );
}

// 获得所在格子的坐标。每个精灵只能占一个格子的位置
// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
BOOL CHero::GetGride( int* pX, int* pY )
{
	int iCenterX = m_x + HERO_CENTER_OFFSET_X;
	int iCenterY = m_y + HERO_CENTER_OFFSET_Y;

	if( (iCenterX < 0) || (iCenterX > TILE_W * GRIDE_W) )
		return FALSE;
	if( (iCenterY < 0) || (iCenterY > TILE_H * GRIDE_H) )
		return FALSE;

	*pX = iCenterX / TILE_W;
	*pY = iCenterY / TILE_H;
	return TRUE;
}

// 得到碰撞检测矩形
BOOL CHero::GetHitTestRect( int* x, int* y, int* w, int* h )
{
	*x = m_x;
	*y = m_y + HERO_H - TILE_H;
	*w = TILE_W;
	*h = TILE_H;
	return TRUE;
}

// 设置动作
BOOL CHero::SetAct( WORD wAct, long lNow )
{
	if( m_wAct == HERO_ACT_DEAD )
		return FALSE;

	// 动作未完成则拒绝接受新的动作
	if( (m_wAct & HERO_ACT_GO_COMPLETE) > 0 )
	{
		switch( wAct )
		{
		case HERO_ACT_GO_UP:
		case HERO_ACT_GO_DOWN:
		case HERO_ACT_GO_LEFT:
		case HERO_ACT_GO_RIGHT:
			{
				m_wAct &= 0xF0;
				m_wAct |= wAct;
				return TRUE;
			}
			break;
		}
	}

	if( (m_wAct & HERO_ACT_OTHER_COMPLETE) > 0 )
	{
		switch( wAct )
		{
		case HERO_ACT_FIRE:
			{
				m_wAct &= ~HERO_ACT_RENEW_CASSETTE;
				m_wAct |= wAct;
				m_lStartTime = lNow;
				return TRUE;
			}
			break;
		case HERO_ACT_RENEW_CASSETTE:
			{
				m_wAct &= ~HERO_ACT_FIRE;
				m_wAct |= wAct;
				// 关闭开火状态
				if( m_nStatus > 40 )
					m_nStatus -= 20;
				m_lStartTime = lNow;
				return TRUE;
			}
			break;
		}
	}

	return FALSE;
}

// 持枪/无枪的设置
BOOL CHero::SetGun( BOOL bHoldGun )
{
	if( bHoldGun )
	{
		if( m_nStatus < 20 )
			m_nStatus += 20;
	}
	else
	{
		if( m_nStatus > 20 )
			m_nStatus %= 20;
	}

	return TRUE;
}

// 测试角色手中是否有枪
BOOL CHero::HaveGun()
{
	if( m_nStatus > 20 )
		return TRUE;
	return FALSE;
}

// m_nStatus;
//  1:无枪，面上迈步1； 21:持枪，面上迈步1； 41:射击，面上迈步1；
//  2:无枪，面上迈步2； 22:持枪，面上迈步2； 42:射击，面上迈步2；
//  3:无枪，面上站立；  23:持枪，面上站立；  43:射击，面上站立；
//  4:无枪，面下迈步1； 24:持枪，面下迈步1； 44:射击，面下迈步1；
//  5:无枪，面下迈步2； 25:持枪，面下迈步2； 45:射击，面下迈步2；
//  6:无枪，面下站立；  26:持枪，面下站立；  46:射击，面下站立；
//  7:无枪，面左迈步1； 27:持枪，面左迈步1； 47:射击，面左迈步1；
//  8:无枪，面左迈步2； 28:持枪，面左迈步2； 48:射击，面左迈步2；
//  9:无枪，面左站立；  29:持枪，面左站立；  49:射击，面左站立；
// 10:无枪，面右迈步1； 30:持枪，面右迈步1； 50:射击，面右迈步1；
// 11:无枪，面右迈步2； 31:持枪，面右迈步2； 51:射击，面右迈步2；
// 12:无枪，面右站立；  32:持枪，面右站立；  52:射击，面右站立；
// 无枪动作图片的y值加192为持枪动作图片，加384为射击动作图片。
// HERO_ACT_REST            0x00              // 无动作
// HERO_ACT_GO_UP           0x01              // 向上走
// HERO_ACT_GO_DOWN         0x02              // 向下走
// HERO_ACT_GO_LEFT         0x03              // 向左走
// HERO_ACT_GO_RIGHT        0x04              // 向右走
// HERO_ACT_UNIQUE_SKILL    0x08              // 绝招
// HERO_ACT_FIRE            0x10              // 开火
// HERO_ACT_CHANGE_CASSETTE 0x20              // 更换弹夹
// HERO_ACT_GO_COMPLET      0x40              // 行走动作执行完成标志
// HERO_ACT_OTHER_COMPLETE  0x80              // 其他动作执行完成标志
// 
// 移动
void CHero::Move( long lNow )
{
	if( m_wAct == HERO_ACT_DEAD )
		return;

	// 将当前动作推进到下一个状态
	// 1>行走动作的推进
	if( (m_wAct & HERO_ACT_GO_COMPLETE) > 0 )
	{
		WORD wAct = m_wAct & 0x07;
		// 行走状态已经结束。
		// 如果没有下一个行走动作，则设置动作为站立等待状态
		if( wAct == 0 )
		{
			if( ((m_nStatus % 20) % 3) != 0 )
				m_nStatus ++;
		}
		else
		{
			int nFaceDirection;
			switch( wAct )
			{
			case HERO_ACT_GO_UP:
				nFaceDirection = 3;
				break;
			case HERO_ACT_GO_DOWN:
				nFaceDirection = 6;
				break;
			case HERO_ACT_GO_LEFT:
				nFaceDirection = 9;
				break;
			case HERO_ACT_GO_RIGHT:
				nFaceDirection = 12;
				break;
			}

			if( ((m_nStatus % 20) > nFaceDirection) ||
				((m_nStatus % 20) < (nFaceDirection - 2)) )
			{
				// 如果不是当前面向则转头
				m_nStatus = 20 * (m_nStatus / 20) + nFaceDirection;
				m_wAct &= 0xF8;
			}
			else
			{
				m_nStatus = 20 * (m_nStatus / 20) + nFaceDirection - 2;
				// 移动到新位置
				MoveToNewPos( wAct );
				// 清除动作完成标志
				m_wAct &= ~HERO_ACT_GO_COMPLETE;
			}
		}
	}
	else
	{
		// 推进到下一个状态
		m_nStatus ++;
		// 移动到新位置
		MoveToNewPos( m_wAct & 0x07 );
		// 执行到走动的第二步，则设置行走动作完成标志并清除动作
		if( ((m_nStatus % 20) % 3) == 2 )
		{
			m_wAct &= 0xF8;
			m_wAct |= HERO_ACT_GO_COMPLETE;
		}
	}

	// 2>更换弹夹和开火动作的推进
	if( (m_wAct & HERO_ACT_RENEW_CASSETTE) > 0 )
	{
		// 更换弹夹动作
		m_wAct &= ~HERO_ACT_OTHER_COMPLETE;
		if( (lNow - m_lStartTime) > RENEW_CASSETTE_DELAY )
		{
			m_wAct &= ~HERO_ACT_RENEW_CASSETTE;
			m_wAct |= HERO_ACT_OTHER_COMPLETE;
		}
	}
	else if( (m_wAct & HERO_ACT_FIRE) > 0 )
	{
		// 开火动作
		if( m_nStatus < 20 )
		{
			// 无枪状态不能开火，
			m_wAct &= ~HERO_ACT_FIRE;
			m_wAct |= HERO_ACT_OTHER_COMPLETE;
		}
		else if( m_nStatus > 40 )
		{
			// 如果正在开火，则关闭开火，并设置动作结束标志
			m_nStatus -= 20;
			m_wAct &= ~HERO_ACT_FIRE;
			m_wAct |= HERO_ACT_OTHER_COMPLETE;
		}
		else if( m_nStatus > 20 )
			// 持枪状态转换为开火状态
			m_nStatus += 20;
	}
}

// 移动到新位置
void CHero::MoveToNewPos( WORD wAct )
{
	int nOld_x = m_x;
	int nOld_y = m_y;

	// (需要增加事件检测)
	int X; int Y;
	if( GetGride( &X, &Y ) )
	{
		switch( wAct )
		{
		case HERO_ACT_GO_UP:
			{
				if( Y > 0 )
					m_y -= WALK_STEP;
			}
			break;
		case HERO_ACT_GO_DOWN:
			{
				if( Y < GRIDE_H - 1 )
					m_y += WALK_STEP;
			}
			break;
		case HERO_ACT_GO_LEFT:
			{
				if( X > 0 )
					m_x -= WALK_STEP;
			}
			break;
		case HERO_ACT_GO_RIGHT:
			{
				if( X < GRIDE_W - 1 )
					m_x += WALK_STEP;
			}
			break;
		}
	}

	BOOL bHitWallTest = m_pGameMap->HitWallTest(this);
	BOOL bEventTest   = m_pGameMap->EventTest(this);

	if( bHitWallTest || bEventTest ||
   		(m_pGameMap->HitTest(this) != NULL) )
	{
		m_x = nOld_x;
		m_y = nOld_y;
		return;
	}

	// 移动地图
	m_pGameMap->MoveMap( m_x - nOld_x, m_y - nOld_y );
}

// 初始化副角色图片设备
BOOL CHero::InitImage2( char* psFileName )
{
	// 打开位图
	HBITMAP hBmp=(HBITMAP)LoadImage(NULL,psFileName,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	if( hBmp == NULL )
	{
		m_hImageDC2 = NULL;
		return FALSE;
	}

	HDC hdc = ::GetDC( theApp.m_pMainWnd->m_hWnd );
	m_hImageDC2 = ::CreateCompatibleDC( hdc );
	DeleteObject( ::SelectObject( m_hImageDC2, hBmp ) );
	ReleaseDC( theApp.m_pMainWnd->m_hWnd, hdc );

	return TRUE;
}

// 判断副角色是否存在
BOOL CHero::HaveHero2()
{
	if( m_hImageDC2 == NULL )
		return FALSE;

	return TRUE;
}

// END