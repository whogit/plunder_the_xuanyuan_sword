//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 mapedit.rc 使用
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_MAPEDITYPE                  129
#define IDR_TOOLBAR                     135
#define IDR_EDIT_AREA_SCROLL_H          140
#define IDR_EDIT_AREA_SCROLL_V          141
#define IDR_MAP_TILES_SCROLL_H          142
#define IDB_CODEMAP                     142
#define IDD_THUMBNAIL                   143
#define IDR_POPMENU                     144
#define IDC_EDT_EMAIL                   1001
#define ID_BLOCK1                       32772
#define ID_BLOCK2                       32773
#define ID_BLOCK3                       32774
#define ID_BLOCK4                       32775
#define ID_INFO                         32779
#define ID_BLOCK0                       32780
#define ID_UP                           32781
#define ID_DOWN                         32782
#define ID_LEFT                         32783
#define ID_RIGHT                        32784
#define ID_SET                          32785
#define ID_MENUITEM32786                32786
#define ID_LOCK                         32787
#define ID_MENUITEM32812                32812
#define ID_MENUITEM32813                32813
#define ID_MENUITEM32814                32814
#define ID_MENUITEM32815                32815
#define ID_QUICK_UP                     32816
#define ID_QUICK_DOWN                   32817
#define ID_QUICK_LEFT                   32818
#define ID_QUICK_RIGHT                  32819
#define ID_PICKUP                       32822
#define ID_BRUSH1                       32823
#define ID_BRUSH2                       32824
#define ID_BRUSH3                       32825
#define ID_BRUSH4                       32826
#define ID_BRUSH5                       32827
#define ID_BRUSH6                       32828
#define ID_COPY                         32829
#define ID_PASTE                        32830
#define ID_BRUSH7                       32831
#define ID_STEP_UP                      32832
#define ID_STEP_DOWN                    32833
#define ID_STEP_LEFT                    32834
#define ID_STEP_RIGHT                   32835
#define ID_ERASE                        32839
#define ID_32840                        32840
#define ID_32841                        32841
#define ID_THUMBNAIL                    32842
#define ID_32843                        32843
#define ID_EDIT_EVENT                   32844
#define ID_32845                        32845
#define ID_OPEN_EVENT_FILE              32846
#define ID_32847                        32847
#define ID_EXPORT_PNG                   32848
#define ID_32849                        32849
#define ID_32850                        32850
#define ID_JUMP_TO_HERE                 32851
#define ID_32852                        32852
#define ID_EXPORT_MINI_PNG              32853

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        145
#define _APS_NEXT_COMMAND_VALUE         32854
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
